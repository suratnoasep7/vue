import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import infiniteScroll from "vue-infinite-scroll"

Vue.use(Vuex);
Vue.use(infiniteScroll);

export default new Vuex.Store({
  state: {},
  actions: {},
  mutations: {}
});
